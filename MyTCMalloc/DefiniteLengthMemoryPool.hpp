#ifndef __DEFINITELENGTH__
#define __DEFINITELENGTH__
// 定长内存池的编写

#include "Common.hpp"



namespace QiHai
{
	template<class T>
	class DefinitePoll
	{
	public:
		// 申请对应大小的内存
		T* New();
		// 回收不要的内存
		void Delete(T* res);
	private:
		char* _systembegin = nullptr;  // 当前申请的内存起始空间
		int _length = 0;  // 当前申请的内存剩余大小
		void* _freeListHead = nullptr;  // 回收释放内存的自由链表
		//int interval_T = sizeof(T);  // 类型的大小
	};

	////////////////DefinitePoll/////////////////////

	template<class T>
	T* DefinitePoll<T>::New()
	{
		T* ptr = nullptr;
		if (_freeListHead)
		{
			// 如果自由链表中存在结点
			ptr = (T*)_freeListHead;
			_freeListHead = NextObj(_freeListHead);
		}
		else
		{
			// 说明自由链表内没有结点
			// 判断剩余的内存还够吗
			if (_length < sizeof(T))
			{
				// 向系统申请1page内存
				_systembegin = (char*)SystemAlloc((size_t)1);
				if (_systembegin == nullptr) return nullptr;
				_length = 1 << 12;  // 1 * 4 * 1024;
			}

			ptr = (T*)_systembegin;
			size_t add = sizeof(T) > sizeof(void*) ? sizeof(T) : sizeof(void*);
			_systembegin += add;
			_length -= add;
		}

		// 首先使用定位new，让对应类型的构造函数调用
		new(ptr)T;
		return ptr;
	}

	template<class T>
	void DefinitePoll<T>::Delete(T* res)
	{
		// 首先显示调用其析构函数，自动调用析构
		res->~T();
		NextObj(res) = _freeListHead;
		_freeListHead = res;
	}
}




#endif // !__FIXDLENGTH__
