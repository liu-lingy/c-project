//#include "DefiniteLengthMemoryPool.hpp"
//#include "ConcurrentPool.hpp"
//#include <ctime>
//#include <vector>
//
//// 单元测试
//
////struct TreeNode
////{
////	int _val;
////	TreeNode* _left;
////	TreeNode* _right;
////	TreeNode()
////		:_val(0)
////		, _left(nullptr)
////		, _right(nullptr)
////	{}
////};
////// 测试定长内存池的效率
////void TestObjectPool()
////{
////	// 申请释放的轮次
////	const size_t Rounds = 3;
////	// 每轮申请释放多少次
////	const size_t N = 100000;
////	size_t begin1 = clock();
////	std::vector<TreeNode*> v1;
////	v1.reserve(N);
////	for (size_t j = 0; j < Rounds; ++j)
////	{
////		for (int i = 0; i < N; ++i)
////		{
////			v1.push_back(new TreeNode);
////		}
////		for (int i = 0; i < N; ++i)
////		{
////			delete v1[i];
////		}
////		v1.clear();
////	}
////	size_t end1 = clock();
////	QiHai::DefinitePoll<TreeNode> TNPool;
////	size_t begin2 = clock();
////	std::vector<TreeNode*> v2;
////	v2.reserve(N);
////	for (size_t j = 0; j < Rounds; ++j)
////	{
////		for (int i = 0; i < N; ++i)
////		{
////			v2.push_back(TNPool.New());
////		}
////		for (int i = 0; i < 100000; ++i)
////		{
////			TNPool.Delete(v2[i]);
////		}
////		v2.clear();
////	}
////	size_t end2 = clock();
////	std::cout << "new cost time:" << end1 - begin1 << std::endl;
////	std::cout << "object pool cost time:" << end2 - begin2 << std::endl;
////}
////
////// 测试类SizeClass
////void TestSizeClass()
////{
////	// 根据映射规则：给出如下几个类型字节大小，算出其对齐后的字节数和对应的哈希桶
////	// 100(8) 104-12
////	// 520(16) 528-40
////	size_t bytes = 520;
////	std::cout << bytes << ":" << QiHai::SizeClass::RoundUp(bytes) << "-" << QiHai::SizeClass::Index(bytes) << std::endl;
////}
////
////// 测试申请流程
////
//// 非多线程环境下申请流程
//void TestAlloc1()
//{
//	void* ptr1 = QiHai::ConcurrentAlloc(1); // 剩余:0 -maxsize 2  _useCount: 1
//	void* ptr2 = QiHai::ConcurrentAlloc(4); // 剩余:1 -maxsize 3  _useCount: 3
//	void* ptr3 = QiHai::ConcurrentAlloc(2); // 剩余:0
//	void* ptr4 = QiHai::ConcurrentAlloc(8); // 剩余:2 -maxsize 4  _useCount: 6
//	void* ptr5 = QiHai::ConcurrentAlloc(7); // 剩余:1
//	void* ptr6 = QiHai::ConcurrentAlloc(5); // 剩余:0
//	void* ptr7 = QiHai::ConcurrentAlloc(6); // 剩余:3 -maxsize 5  _useCount: 10
//
//	void* res1 = QiHai::ConcurrentAlloc(256);
//	void* res2 = QiHai::ConcurrentAlloc(56 << QiHai::SHIFT_PAGE);  // 56page - 
//	void* res3 = QiHai::ConcurrentAlloc(129 << QiHai::SHIFT_PAGE);
//
//
//	QiHai::ConcurrentFree(ptr1);
//	QiHai::ConcurrentFree(ptr2);
//	QiHai::ConcurrentFree(ptr3);
//	QiHai::ConcurrentFree(ptr4);
//	QiHai::ConcurrentFree(ptr5);
//	QiHai::ConcurrentFree(ptr6);
//	QiHai::ConcurrentFree(ptr7);
//
//	QiHai::ConcurrentFree(res1);
//	QiHai::ConcurrentFree(res2);
//	QiHai::ConcurrentFree(res3);
//
//}
//
//// 单元测试
//int main()
//{
//	//TestObjectPool();
//	//TestSizeClass();
//	TestAlloc1();
//
//	return 0;
//}