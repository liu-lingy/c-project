#ifndef __LOG_HPP__
#define __LOG_HPP__
// 日志功能
#include <iostream>
#include <string>
#include "./util.hpp"

namespace ns_log
{
    using namespace ns_util;

    // 日志等级
    enum{
        INFO,    // 正常
        DEBUG,   // 调试
        WARING,  // 警告
        ERROR,   // 错误
        FATAL    // 致命
    };

    // 开放式输出日志信息
    inline std::ostream& Log(const std::string& level, const std::string& file_name, int line)
    {
        std::string log;
        // 添加日志等级
        log += "[";
        log += level;
        log += "]";

        // 添加输出日志文件名
        log += "[";
        log += file_name;
        log += "]";

        // 添加日志行号
        log += "[";
        log += std::to_string(line);
        log += "]";

        // 添加当前日志时间戳
        log += "(";
        log += TimeUtil::GetTimeStamp();
        log += ") ";

        std::cout << log;  // 不使用endl进行刷新，利用缓冲区 开放式日志文件

        return std::cout;
    }
    // 利用宏定义完善一点 - LOG(INFO) << "一切正常\n";
    #define LOG(level) Log(#level, __FILE__, __LINE__)
}
#endif