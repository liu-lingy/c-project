#include <iostream>
#include <string>
#include <signal.h>
#include "../common/httplib.h"
#include "oj_control.hpp"

using namespace httplib;
using namespace ns_control;

static Control* _ctl = nullptr;
void AllOnline(int sig)
{
    _ctl->AllOnLineMachine();
}

// 用户路由
int main()
{
    Server svr;  // 服务器对象
    Control ctl;  // 逻辑控制对象

    _ctl = &ctl;
    signal(3, AllOnline);  // 捕捉三号信号 ctrl \ 可以重新上线操作

    // 1.用户获取题目列表数据
    svr.Get("/all_questions", [&ctl](const Request& req, Response& resp){
        std::string html;
        ctl.AllQuestions(html);
        resp.set_content(html, "text/html;charset=utf8");
    });

    // 2.用户获取单题题目内容
    svr.Get(R"(/question/(\d+))", [&ctl](const Request& req, Response& resp){  // (\\d+是正则表达式)
        std::string number = req.matches[1];  // 1对应的就是获取正则表达式中的内容
        std::string html;
        ctl.OneQuestion(number, html);
        resp.set_content(html, "text/html;charset=utf8");
    });

    // 3.用户获取单体解析结果
    svr.Post(R"(/judge/(\d+))", [&ctl](const Request& req, Response& resp){
        std::string number = req.matches[1];  // 1对应的就是获取正则表达式中的内容
        std::string out_json;
        ctl.Judge(number, req.body, out_json);
        resp.set_content(out_json, "application/json;charset=utf8");
    });

    // 扩展1：用户管理系统
    svr.Post("/user", [&ctl](const Request& req, Response& resp){
        std::string out_json;
        ctl.user_control(req.body, out_json, resp);
        resp.set_content(out_json, "application/json;charset=utf8");
    });

    // 设置根目录
    svr.set_base_dir("./wwwroot");
    // 监听全体网卡，端口号固定8080
    svr.listen("0.0.0.0", 8080);
    return 0;
}