#ifndef COMPILE_ONLINE
#include "header.cpp"  // 条件编译，引入头文件只是为了不报错和语法提示，之后会进行拼接
#endif
#include <iostream>

int main()
{
    UnusualAdd ues;
    int a, b;
    while (cin >> a >> b)
    {
        cout << ues.addAB(a, b) << "\n";
    }
    return 0;
}