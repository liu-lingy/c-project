#ifndef COMPILE_ONLINE
#include "header.cpp"  // 条件编译，引入头文件只是为了不报错和语法提示，之后会进行拼接
#endif

void test1(UnusualAdd& use1)
{
    // 测试用例1
    int sum = use1.addAB(1, 1);
    if (sum == 2)
    {
        cout << "测试用例1通过!" << endl; 
    }
    else cout << "测试用例1没有通过"<< "1 + 1" << endl;
}

void test2(UnusualAdd& use2)
{
    // 测试用例2
    int sum = use2.addAB(1, -1);
    if (sum == 0)
    {
        cout << "测试用例2通过!" << endl; 
    }
    else cout << "测试用例2没有通过" << "1 + (-1)" << endl;
}

int main()
{
    UnusualAdd ues;
    test1(ues);
    test2(ues);
    return 0;
}