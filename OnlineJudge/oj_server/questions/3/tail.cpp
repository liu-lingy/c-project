#ifndef COMPILE_ONLINE
#include "header.cpp"  // 条件编译，引入头文件只是为了不报错和语法提示，之后会进行拼接
#endif

void test1(Solution& use1)
{
    // 测试用例1
    vector<int> nums = {4, 5, 6, 7, 0, 1, 2};
    int sum = use1.search(nums, 0);
    if (sum == 4)
    {
        cout << "测试用例1通过!" << endl; 
    }
    else cout << "测试用例1没有通过" << endl;
}

void test2(Solution& use1)
{
    // 测试用例2
    vector<int> nums = {4, 5, 6, 7, 0, 1, 2};
    int sum = use1.search(nums, 3);
    if (sum == -1)
    {
        cout << "测试用例2通过!" << endl; 
    }
    else cout << "测试用例2没有通过" << endl;
}

void test3(Solution& use1)
{
    // 测试用例3
    vector<int> nums = {1};
    int sum = use1.search(nums, 0);
    if (sum == -1)
    {
        cout << "测试用例3通过!" << endl; 
    }
    else cout << "测试用例3没有通过" << endl;
}

int main()
{
    Solution ues;
    test1(ues);
    test2(ues);
    test3(ues);
    return 0;
}