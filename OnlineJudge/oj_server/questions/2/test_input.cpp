#ifndef COMPILE_ONLINE
#include "header.cpp"  // 条件编译，引入头文件只是为了不报错和语法提示，之后会进行拼接
#endif

int main()
{
    Solution ues;
    string str;
    while (cin >> str)
    {
        cout << ues.StrToInt(str) << std::endl;
    }
    return 0;
}