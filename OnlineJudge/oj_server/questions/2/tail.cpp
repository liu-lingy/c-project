#ifndef COMPILE_ONLINE
#include "header.cpp"  // 条件编译，引入头文件只是为了不报错和语法提示，之后会进行拼接
#endif

void test1(Solution& use1)
{
    // 测试用例1
    string s1 = "+2147483647";
    int sum = use1.StrToInt(s1);
    if (sum == 2147483647)
    {
        cout << "测试用例1通过!" << endl; 
    }
    else cout << "测试用例1没有通过 "<< s1 << endl;
}

void test2(Solution& use2)
{
    // 测试用例2
    string s2 = "1a33";
    int sum = use2.StrToInt(s2);
    if (sum == 0)
    {
        cout << "测试用例2通过!" << endl; 
    }
    else cout << "测试用例2没有通过 " << s2 << endl;
}

int main()
{
    Solution ues;
    test1(ues);
    test2(ues);
    return 0;
}