var userProfileModal = document.getElementById("userProfileModal");
var loginModal = document.getElementById("loginModal");
var loginBtn = document.getElementById("loginBtn");
var closeBtns = document.getElementsByClassName("close");
var registerBtn = document.getElementById("registerBtn");
var registerModal = document.getElementById("registerModal");
var changePasswordModal = document.getElementById("changePasswordModal");

// 点击登录按钮显示登录弹窗
loginBtn.onclick = function() {
    var isLoggedIn = checkUserLoginStatus(); // 检查用户登录状态的函数，返回 true 或 false

    if (isLoggedIn) {
        // 用户已登录，显示个人信息弹窗
        userProfileModal.style.display = "block";
    } else {
        // 用户未登录，显示登录弹窗
        loginBtn.innerHTML = "登录";
        loginModal.style.display = "block";
    }
}

// checkUserLoginStatus 函数：检查用户是否已登录，基于 cookie 实现
function checkUserLoginStatus() {
    // 获取浏览器中的 cookie 字符串
    var cookies = document.cookie;

    // 将 cookie 字符串拆分成键值对
    var cookieArr = cookies.split('; ');

    // 遍历所有 cookie，查找是否存在名为 "session_id" 的 cookie
    for (var i = 0; i < cookieArr.length; i++) {
        var cookie = cookieArr[i].split('=');
        if (cookie[0] === 'session_id') {
            loginBtn.innerHTML = localStorage.getItem('username');
            return true;
        }
    }

    // 没有找到 session_id，表示用户未登录
    return false;
}

// 当页面加载时调用checkUserLoginStatus()函数，如果用户已经登录，则更新登录按钮的内容
document.addEventListener('DOMContentLoaded', checkUserLoginStatus);

// 点击注册按钮显示注册弹窗
registerBtn.onclick = function() {
    event.preventDefault(); // 阻止表单默认提交行为
    loginModal.style.display = "none";
    registerModal.style.display = "block";
}

// 关闭按钮点击事件
for (var i = 0; i < closeBtns.length; i++) {
    closeBtns[i].onclick = function() {
        changePasswordModal.style.display = "none";
        registerModal.style.display = "none";
        userProfileModal.style.display = "none";
        loginModal.style.display = "none";
    }
}

// 点击屏幕外取消弹出窗
// window.onclick = function(event) {
//     if (event.target == loginModal) {
//         loginModal.style.display = "none";
//     } else if (event.target == registerModal) {
//         registerModal.style.display = "none";
//     }
// }

// 清除当前网站的cookie
function clearAllCookies() {
    // 获取当前网站所有的Cookie
    var cookies = document.cookie.split(";");
  
    // 遍历所有Cookie，并将它们过期时间设置为过去的时间，从而清除它们
    for (var i = 0; i < cookies.length; i++) {
      var cookie = cookies[i];
      var eqPos = cookie.indexOf("=");
      var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/";
    }
}

var captcha = "";  // 验证码
// 生成随机的4位验证码
function generateCaptcha() {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    captcha = "";
    for (var i = 0; i < 4; i++) {
        var randomIndex = Math.floor(Math.random() * chars.length);
        captcha += chars.charAt(randomIndex);
    }
    
    // 在控制台输出验证码（可选）
    console.log("验证码：" + captcha);
    
    // 获取Canvas元素并绘制验证码
    var canvas = document.getElementById("captchaCanvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.font = "20px Arial";
    ctx.fillStyle = "black";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(captcha, canvas.width / 2, canvas.height / 2);
}

// 修改密码按钮点击事件处理
function changePassword() {
    changePasswordModal.style.display = "block";
    generateCaptcha();
}

// 退出登录按钮点击事件处理
function logout() {
    // 将浏览器保留的cookie摧毁
    clearAllCookies();
    loginBtn.innerHTML = "登录";
    userProfileModal.style.display = "none";  // 关闭弹窗即可
}

// 提交登录注册信息，后端交互检测
// 登录表单提交事件处理
loginForm.onsubmit = function(event) {
    event.preventDefault(); // 阻止表单默认提交行为

    username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    // 在这里执行登录操作，例如发送登录请求到服务器进行验证
    // console.log("用户名:", username);
    // console.log("密码:", password);

    // 根据登录结果执行相应操作
    // 对应路由地址
    var user_url = "/user";
    // 构建json，向服务器发送用户信息提交请求-使用post，不会再地址栏中进行显示

    $.ajax({
        method: 'Post',
        url: user_url,
        dataType: 'json',  // 我们需要对方返回的类型
        contentType: 'application/json;charset=utf8',  // 我向对方发送数据类型
        data: JSON.stringify({
            'login': true,
            'changePassword': false,
            'username': username,
            'password': password
        }),
        success: function(data){  // 成功后执行此匿名函数
            // console.log(data);
            test_result(data);
        }
    });

    // 得到结果，解析到result中去。
    function test_result(data)
    {
        // 先提取json结果：reason
        var reason = data.reason;
        var user_error = $("#loginModal .modal-content form .user_error");
        var password_error = $("#loginModal .modal-content form .password_error");
        //0 登录成功 1 内部错误 2 用户名不存在或者错误 3 密码错误 4权限错误
        if (reason == 0)
        {
            alert("登录成功，欢迎用户" + username);

            loginBtn.innerHTML = username;
            localStorage.setItem('username', username);  // 持久化到浏览器中保存

            // 清空表单字段
            user_error.empty();
            password_error.empty();
            document.getElementById("username").value = "";
            document.getElementById("password").value = "";
            loginModal.style.display = "none";  // 关闭弹窗
        }
        else if (reason == 2)
        {
            // console.log("用户名不存在或者错误");
            user_error.empty();
            password_error.empty();
            var res = $("<pre>",{
                text: "用户名不存在或者错误"
            });
            res.appendTo(user_error);
            document.getElementById("username").value = "";
            document.getElementById("password").value = "";
        }
        else if (reason == 3)
        {
            // console.log("密码输入错误");
            user_error.empty();
            password_error.empty();
            var res = $("<pre>",{
                text: "密码输入错误"
            });
            res.appendTo(password_error);
            document.getElementById("password").value = "";
        }
    }

    // 关闭登录弹窗
    // loginModal.style.display = "none";
}

// 注册弹窗提交
registerFrom.onsubmit = function(event) {
    event.preventDefault(); // 阻止表单默认提交行为

    var regUsername = document.getElementById("regUsername").value;
    var regPassword = document.getElementById("regPassword").value;

    // 在这里执行注册操作，例如发送登录请求到服务器进行验证
    // console.log("用户名:", regUsername);
    // console.log("密码:", regPassword);

    var user_url = "/user";

    $.ajax({
        method: 'Post',
        url: user_url,
        dataType: 'json',  // 我们需要对方返回的类型
        contentType: 'application/json;charset=utf8',  // 我向对方发送数据类型
        data: JSON.stringify({
            'login': false,
            'username': regUsername,
            'password': regPassword
        }),
        success: function(data){  // 成功后执行此匿名函数
            // console.log(data);
            test_result(data);
        }
    });

    // 得到结果，解析到result中去。
    function test_result(data)
    {
        // 先提取json结果：reason
        var reason = data.reason;
        var user_error = $("#registerModal .modal-content form .user_error");
        var password_error = $("#registerModal .modal-content form .password_error");
        //0 注册成功 1 内部错误 2 用户名为空或者已经存在 3 密码格式错误
        if (reason == 0)
        {
            user_error.empty();
            password_error.empty();
            // 清空表单字段
            document.getElementById("username").value = "";
            document.getElementById("password").value = "";
            alert("注册成功，欢迎用户" + regUsername);
            loginModal.style.display = "none";  // 关闭弹窗
        }
        else if (reason == 2)
        {
            // console.log("用户名不存在或者错误");
            user_error.empty();
            password_error.empty();
            var res = $("<pre>",{
                text: "用户名为空或者已经存在"
            });
            res.appendTo(user_error);
            document.getElementById("username").value = "";
            document.getElementById("password").value = "";
        }
        else if (reason == 3)
        {
            // console.log("密码输入错误");
            user_error.empty();
            password_error.empty();
            var res = $("<pre>",{
                text: "密码格式错误"
            });
            res.appendTo(password_error);
            document.getElementById("password").value = "";
        }
    }

}

// 修改密码弹窗提交
changePasswordFrom.onsubmit = function(event) {
    event.preventDefault(); // 阻止表单默认提交行为

    var changeCaptcha = document.getElementById("verCode").value;  // 获取输入的验证码
    var verCode_error = $("#changePasswordModal .modal-content form .verCode_error");
    if (changeCaptcha != captcha)
    {
        // 验证码错误，刷新验证码需要重新输入
        generateCaptcha();
        verCode_error.empty();
        var res = $("<pre>",{
            text: "验证码输入错误"
        });
        res.appendTo(verCode_error);
        return;
    }
    else verCode_error.empty();
    // 验证码输入成功，验证密码

    var oldPassword = document.getElementById("oldPassword").value;  // 获取输入的旧密码
    var newPassword1 = document.getElementById("newPassword1").value;  // 获取第一次输入的新密码
    var newPassword2 = document.getElementById("newPassword2").value;  // 获取第二次输入的新密码
    
    // 错误提醒
    var oldPassword_error = $("#changePasswordModal .modal-content form .oldPassword_error");
    var newPassword_error = $("#changePasswordModal .modal-content form .newPassword_error");
    var password_error = $("#changePasswordModal .modal-content form .password_error");

    if (newPassword1 != newPassword2)
    {
        // 前后密码输入不一致，重新输入
        generateCaptcha();  // 刷新验证码
        password_error.empty();
        var res = $("<pre>",{
            text: "和第一次输入新密码不一致"
        });
        res.appendTo(password_error);
        document.getElementById("newPassword1").value = "";
        document.getElementById("newPassword2").value = "";
        return;
    }
    else password_error.empty();

    var user_url = "/user";
    // 构建json，向服务器发送用户信息提交请求-使用post，不会再地址栏中进行显示

    $.ajax({
        method: 'Post',
        url: user_url,
        dataType: 'json',  // 我们需要对方返回的类型
        contentType: 'application/json;charset=utf8',  // 我向对方发送数据类型
        data: JSON.stringify({
            'login': true,
            'changePassword': true,
            'username': localStorage.getItem('username'),
            'password': oldPassword,
            'newPassword': newPassword1
        }),
        success: function(data){  // 成功后执行此匿名函数
            test_result(data);
        }
    });

    // 得到结果，解析到result中去。
    function test_result(data)
    {
        var reason = data.reason;
        if (reason == 0)
        {
            // 修改密码成功
            oldPassword_error.empty();
            newPassword_error.empty();
            alert("修改密码成功！");
            changePasswordModal.style.display = "none";
        }
        else if (reason == 3)
        {
            // 旧密码输入错误
            oldPassword_error.empty();
            var res = $("<pre>",{
                text: "旧密码输入错误"
            });
            res.appendTo(oldPassword_error);
            document.getElementById("oldPassword").value = "";
            document.getElementById("newPassword1").value = "";
            document.getElementById("newPassword2").value = "";
        }
        else if (reason == -1)
        {
            // 新密码输入格式错误
            newPassword_error.empty();
            var res = $("<pre>",{
                text: "新密码输入格式错误"
            });
            res.appendTo(newPassword_error);
            document.getElementById("newPassword1").value = "";
            document.getElementById("newPassword2").value = "";
        }
    }


}