#include <iostream>
#include <string>
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ctemplate/template.h>  // 引入网络渲染库

using namespace std;

// void handler(int sig)
// {
//     cout << "终止信号为：" << sig << endl;
//     exit(sig);
// }

// void TimeLimit()
// {
//     // CPU时间限制
//     // 限制3s
//     struct rlimit r;
//     r.rlim_cur = 3;  // 3s
//     r.rlim_max = RLIM_INFINITY;  // 无限制
//     setrlimit(RLIMIT_CPU, &r);

//     while(1);
// }

// void MemoryLimit()
// {
//     // 内存空间限制
//     // 限制40mb
//     struct rlimit r;
//     r.rlim_cur = 1024 * 1024 * 40;
//     r.rlim_max = RLIM_INFINITY;

//     setrlimit(RLIMIT_AS, &r);
//     int count = 1;
//     while (1)
//     {
//         int* temp = new int[1024 * 1024];  // 4mb
//         cout << "申请了：" << ((count++) * 4) <<  "mb" << endl;
//         sleep(1);
//     }
// }

int main()
{
    // 网络渲染测试
    std::string in_html;
    ctemplate::TemplateDictionary root("test");  // 创建数据字典
    root.SetValue("key", "你好呀，这里是测试代码，看到这行的时候表示已经渲染成功啦");// 插入数据

    // 导入待渲染的网页
    ctemplate::Template *tql = ctemplate::Template::GetTemplate("./test.html", ctemplate::DO_NOT_STRIP);  // 路径  保持原貌代码
    
    // 渲染网页
    std::string out_html;
    tql->Expand(&out_html, &root);

    //std::cout << out_html << std::endl;

    // 捕捉信号查看几号信号终止
    // for (int i = 1; i <= 31; ++i) signal(i, handler);
    // 资源限制测试
    // TimeLimit();  // 时间限制
    // MemoryLimit();  // 空间限制
    return 0;
}
