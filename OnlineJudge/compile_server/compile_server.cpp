#include "compile_run.hpp"
#include "../common/httplib.h"

using namespace ns_compile_and_run;
using namespace httplib;

void User(char * use)
{
    std::cout << "User:" << "\n\t";
    std::cout << use << " port" << std::endl;
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        // 满足传递 ./ port
        User(argv[0]);
        return 1;
    }
    // 将我们的编译运行服务打包为网络服务 - 为负载均衡做准备
    Server svr;
    svr.Get("/hello", [](const Request& req, Response& resp){
        resp.set_content("你好呀，这里是编译运行服务，访问服务请访问资源/compile_and_run", "text/plain;charset=utf-8");
    });  // 请求资源
    svr.Post("/compile_and_run", [](const Request& req, Response& resp){
        // 首先提取用户提交的json串
        std::string in_json = req.body;
        std::string out_json;
        // 判断json串是空的吗？空的拒绝服务
        if(!in_json.empty())
            CompileAndRun::Start(in_json, &out_json);
        // 返回out_json串给客户端
        resp.set_content(out_json, "application/json;charset=utf-8");
    });  // 提交json串，返回json串
    // 注册运行
    svr.listen("0.0.0.0", atoi(argv[1]));



    // 测试cpp-httplib
    // Server svr;
    // svr.Get("/test", [](const Request& req, Response& resp){
    //     resp.set_content("hello~,this is test!", "Content-type: text.plain;");
    // });
    // svr.set_base_dir("./wwwroot/");
    // svr.listen("0.0.0.0", 8080);

    // 测试编译运行模块
    // Json::Value test;
    // test["code"] = R"(#include <iostream>
    // int main()
    // {
    //     std::cout << "hello word!" << std::endl;
    //     //int* test = new int[1024*1024*10];
    //     //int a = 1 / 0;
    //     //while(1);
    //     //hhh
    // })";
    // test["input"] = "";
    // test["cpu_limit"] = 1;
    // test["mem_limit"] = 1024 * 30;  // 30mb
    // Json::FastWriter in;
    // std::string out_json;
    // std::string in_json = in.write(test);
    // std::cout << in_json << std::endl;
    // CompileAndRun::Start(in_json, &out_json);
    // std::cout << out_json << std::endl;
    return 0;
}