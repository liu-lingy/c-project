#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    double findMedianSortedArrays(vector<int>& nums1, vector<int>& nums2) {
    }
};
#ifndef COMPILE_ONLINE
#include "header.cpp"  // 条件编译，引入头文件只是为了不报错和语法提示，之后会进行拼接
#endif

void test1(Solution& use1)
{
    // 测试用例1
    vector<int> nums1 = {1, 3};
    vector<int> nums2 = {2};
    double sum = use1.findMedianSortedArrays(nums1, nums2);
    if (sum == 2.00000)
    {
        cout << "测试用例1通过!" << endl; 
    }
    else cout << "测试用例1没有通过" << endl;
}

void test2(Solution& use1)
{
    // 测试用例2
    vector<int> nums1 = {1, 2};
    vector<int> nums2 = {3, 4};
    double sum = use1.findMedianSortedArrays(nums1, nums2);
    if (sum == 2.50000)
    {
        cout << "测试用例2通过!" << endl; 
    }
    else cout << "测试用例2没有通过" << endl;
}

int main()
{
    Solution ues;
    test1(ues);
    test2(ues);
    return 0;
}