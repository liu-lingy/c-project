# 项目：  
>>* 在线Oj + 负载均衡  
# 模块：  
## 1.common公共模块  
>>* util.hpp 实用  
>>* log.hpp 日志  
>>* httplib.h http连接库  
## 2.compile_server编译运行模块  
>>* |*temp*| 临时文件区  
>>* compiler.hpp 编译模块  
>>* runner.hpp 运行模块  
>>* compile_run.hpp 编译运行整合模块  
>>* compile_server.cc 提供网络接口、源文件  
## 3.oj_server对外提供服务，对内负载均衡调用模块  
>>* |*questions*| OJ题库-文件版本  
>>>>* question.list 综合配置文件  
>>>>* |*1*|  
>>>>>>* desc.txt 题目描述文件  
>>>>>>* header.cpp 提供给用户的预设代码  
>>>>>>* tail.cpp 测试用例,最后和header.cpp组合成最终的cpp文件  
>>>>* |*2*|  
>>>>* |...  
>>* |*wwwroot*| 网页代码提供处  
>>>>* |*css*| css文件存放处  
>>>>* |*js*| js文件存放处  
>>>>* |*images*| 首页图片资源处  
>>>>* index.html  首页代码  
>>* mysql-connect mysql连接软连接库  
>>* template_html 网页带渲染模板html  
>>>>* one_question.html 单个题目渲染  
>>>>* all_questions.html 题库页面渲染  
>>* oj_server.cpp 用户路由，获取资源网络入口  
>>* oj_model.hpp 题库数据交互入口-对题库进行增删改查 M-(文件版本、mysql版本)  
>>* oj_view.hpp 拿到题目数据，渲染网页，单题目交互入口  
>>* oj_control.hpp 整体控制流程，将compileandrun功能与另外两个功能整合控制起来，完成在线OJ的设计  
## 4.test.cpp  测试源码  